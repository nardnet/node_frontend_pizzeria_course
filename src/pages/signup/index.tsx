import { use, useState, FormEvent, useContext } from "react"
import Head from "next/head"
import Image from "next/image"
import Link from "next/link"

import logoImg from '../../../public/logo.svg'
import styles from '../../styles/home.module.scss'

import { Input } from '../../components/ui/Input'
import { Button } from '../../components/ui/Button'
import { AuthContext } from "@/contexts/AuthContext"

export default function Signup() {
  const { signUp } = useContext(AuthContext);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword]= useState('');
  const [loading, setLoading] = useState(false);


  async function handleSignUp(event: FormEvent){
    event.preventDefault();

    if(name === '' || email === '' || password === ''){
      return;
    }

    setLoading(true);

    let data = {
      name,
      email,
      password
    }

    await signUp(data);

    setLoading(false);

  }

  return (
    <>
      <Head>
        <title>Sujeito Pizza - Sign-up</title>
      </Head>
      <div className={styles.containerCenter}>
        <Image src={logoImg} alt="Logo Sujeito Pizzaria"></Image>

        <div className={styles.login}>
          <h1>New account</h1>

          <form onSubmit={handleSignUp}>
            <Input placeholder="Name" type="text" value={name} onChange={(e) => setName(e.target.value) } />

            <Input placeholder="Email" type="text" value={email} onChange={(e) => setEmail(e.target.value) } />

            <Input placeholder="password" type="password" value={password} onChange={(e) => setPassword(e.target.value) } />

            <Button
              type="submit"
              loading={loading}>Sign up
            </Button>
          </form>

          <Link href="/" legacyBehavior>
            <a className={styles.text}>I already have an account</a>
          </Link>

        </div>
      </div>
    </>
  )
}
