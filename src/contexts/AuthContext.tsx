import { createContext, ReactNode, useState } from "react";
import { destroyCookie, setCookie, parseCookies } from "nookies"
import Router from "next/router";

import { api } from "@/services/apiClient";

type AuthContextData = {
    user?: UserProps;
    isAuthenticated: boolean;
    signIn: (credentials: SigInProps) => Promise<void>;
    signOut: () => void;
    signUp: (credentials: SingUpProps) => Promise<void>;
}

type UserProps = {
    id: string;
    name: string;
    email: string;
}

type SigInProps = {
    email: string;
    password: string;
}

type AuthProviderProps = {
    children: ReactNode;
}

type SingUpProps = {
    name: string,
    email: string,
    password: string
}

export const AuthContext = createContext({} as AuthContextData)

export function AuthProvider({ children }: AuthProviderProps) {

    const [user, setUser] = useState<UserProps>()
    const isAuthenticated = !!user;

    async function signIn({ email, password }: SigInProps) {
        try {
            const response = await api.post('/session', {
                email,
                password
            })

            //console.log(response.data);

            const { id, name, token } = response.data;

            setCookie(undefined, '@nextauth.token', token, {
                maxAge: 60 * 60 * 24 * 30,
                path: "/" // which path has permission in the cookies
            });

            setUser({
                id, name, email
            })

            //For the new requests
            api.defaults.headers['Authorization'] = `Bearer ${token}`

            //redirect user to /dasboard
            Router.push('/dashboard')


        } catch (err) {
            console.log("ERROR SIGNIN ", err)
        }
    }

    async function signUp({ name, email, password }: SingUpProps) {
        try {

            const response = await api.post('/users', {
                name, 
                email,
                password
            })
            
            Router.push('/');
        } catch (err) {
            console.log("ERROR SINGUP ", err)
        }
    }

    return (
        <AuthContext.Provider value={{ user, isAuthenticated, signIn, signOut, signUp }}>
            {children}
        </AuthContext.Provider>
    )
}

export function signOut() {
    try {
        destroyCookie(undefined, '@nextauth.token')
        Router.push('/')
    } catch (error) {
        console.log("Error ao deslogar")
    }
}